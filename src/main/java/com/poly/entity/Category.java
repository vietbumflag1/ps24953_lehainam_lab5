package com.poly.entity;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "Categories")
public class Category {
    @Id
    private String id;
    private String name;
}
